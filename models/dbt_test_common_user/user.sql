WITH
administrator AS (
  SELECT DISTINCT
    admin_user_id
  FROM {{ ref('organisation') }}
)

SELECT DISTINCT
  app_user.id AS user_id,
  app_user.created_at AS registered_at,
  team_invite.id IS NOT NULL AS is_invited,
  administrator.admin_user_id IS NOT NULL AS is_organisation_admin,
  'azerty' AS some_value,
  TRUE AS some_bool
FROM `application.user` app_user
LEFT JOIN `common_role.role` role
  ON app_user.id = role.user_id
  AND role.rank_asc_organisation_per_user = 1
LEFT JOIN application.team_invite
  ON app_user.encrypted_email = team_invite.invited_email
  AND role.organisation_id = team_invite.organisation_id
LEFT JOIN administrator
  ON administrator.admin_user_id = app_user.id
