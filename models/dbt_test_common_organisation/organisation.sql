WITH
first_user AS (
  SELECT DISTINCT
    organisation_id,
    FIRST_VALUE(user_id) OVER(PARTITION BY organisation_id ORDER BY created_at) user_id,
  FROM {{ source('application', 'role') }}
  WHERE type <> 'SUPPORT'
),

internal_org AS (
  SELECT
    internal_org_id
  FROM UNNEST([
    -- Used to exclude LIBEO, HYDROGEN, JUNIOR CONSULTING, HOLDEO, PJD orgs from analysis if needed
    '4ed75fb9-6548-4483-8509-fff6254524dc',
    '8fb3194a-0fd6-4e1a-8962-269b14a648d5',
    'b03b96eb-358d-4888-b259-696e75eeac75',
    '960953ee-0601-437a-9d24-b256495b501b',
    '9d47dcdc-355f-48f2-9e23-bdc3a4d4dd42'
  ]) AS internal_org_id
),

kyc AS (
  SELECT DISTINCT
    org.id AS organisation_id,
    kyc.kyc_status,
    kyc.kyc_step,
    MAX(CASE WHEN kyc.kyc_status = 'VALIDATED' THEN COALESCE(comment.timestamp,kyc.kyc_payin_received_at) END) OVER (PARTITION BY org.id) AS kyc_validated_at
  FROM `application.organisation` org
  LEFT JOIN `common_organisation.company_kyc` kyc
    ON org.company_id = kyc.company_id
  LEFT JOIN UNNEST(kyc.kyc_comment) comment
),

kyc_started AS (
  SELECT
    organisation.id AS organisation_id,
    COALESCE(
      MAX(user_event_organisation.timestamp),
      MAX(user_event_company.timestamp),
      MIN(treezor_company_document.created_at)
    ) AS kyc_started_at
  FROM `application.organisation` organisation
  LEFT JOIN `common_user.user_event` user_event_organisation
    ON JSON_EXTRACT_SCALAR(user_event_organisation.event_details,'$.organisation_id') = organisation.id
    AND user_event_organisation.event = 'user_started_kyc'
  LEFT JOIN `common_user.user_event` user_event_company
    ON JSON_EXTRACT_SCALAR(user_event_company.event_details,'$.company_id') = organisation.company_id
    AND user_event_company.event = 'user_started_kyc'
  LEFT JOIN `application.treezor_company_document` treezor_company_document
    ON organisation.company_id = treezor_company_document.company_id
  GROUP BY organisation_id
)

SELECT
  -- organisation general information
  org.id AS organisation_id,
  org.company_id AS company_id,
  REPLACE(REPLACE(company.naf, ".", "")," ","") AS naf_code,   --Uniformize naf codes by removing points and spaces
  org.id in (SELECT * FROM internal_org) as is_internal,
  org.name AS organisation_name,
  org.created_at,
  org.pricing_plan,
  pricing_history.premium_tier,
  org.user_id AS admin_user_id,
  first_user.user_id AS claimer_user_id,
  org.provisionning_strategy AS provisioning_strategy,
  kyc.kyc_status,
  kyc.kyc_step,
  kyc_started.kyc_started_at,
  kyc.kyc_validated_at,
  org.partnership AS partner,
FROM application.organisation org
LEFT JOIN first_user
  ON first_user.organisation_id = org.id
LEFT JOIN  kyc
  ON org.id = kyc.organisation_id
LEFT JOIN kyc_started
  ON org.id = kyc_started.organisation_id
LEFT JOIN `common_organisation.pricing_plan_history` pricing_history
  ON org.id = pricing_history.organisation_id
  AND pricing_history.ended_at IS NULL
LEFT JOIN application.company company
  ON org.company_id = company.id
WHERE first_user.user_id IS NOT NULL  -- Exclude organisations that have only SUPPORT roles
