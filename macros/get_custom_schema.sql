{% macro generate_schema_name(custom_schema_name, node) -%}

    {%- set default_schema = target.schema -%}
    {%- if custom_schema_name is not none -%}
        {{ default_schema }}__{{ custom_schema_name | trim}}
    {%- else -%}
        {{ default_schema }}__{{ node.path.split("/")[0] | trim }}
    {%- endif -%}

{%- endmacro %}
